import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PlayerTest {
    private Player player;
    @Before
    public void setUp(){
        player = new Player("Player2");
    }

    @Test
    public void should_get_current_score_is_equal_0() {
        assertEquals(player.getCurrentScore(), 0);
    }

    @Test
    public void should_return_player_name_is_not_null() {
        assertNotEquals(player.getName(), null);
    }
}