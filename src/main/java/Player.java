import java.util.List;

public class Player {
    public static final List<Integer> gameScore = List.of(0, 15, 30, 40);

    private String name;

    private int currentScore;

    public Player(String name) {
        this.name = name;
        this.currentScore = 0;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void score(){
        this.currentScore++;
    }

    public int tennisScore(){
        return gameScore.get(currentScore);
    }

    public String getName() {
        return name;
    }
}
