Description:
Dans ce kata, vous vous focaliserez sur le score d'un set de Tennis.
Prérequis:
Ecrire un programme qui prend en compte ces éléments pour le tableau des scores entre deux joueurs de Tennis.
 
Les joueurs doivent être capables de marquer des points.
Le set doit être fini avec un gagnant.
Après qu'une partie soit gagnée, le joueur gagnant doit être déterminé.
On doit pouvoir suivre le score de chaque joueur à tout moment durant la partie.

User Story 1 :
As a tennis referee
I want to manage the score of a game of a set of a tennis match between 2 players with simple Game rules
In order to display the current Game score of each player
 
Rules details:
The game starts with a score of 0 point for each player
Each time a player win a point, the Game score changes as follow:
0 -> 15 -> 30 -> 40-> Win game
Example:
GAME SCORE
Start the game	Player 1 wins 1 point	Player 1 wins 1 point	Player 2 wins 1 point	Player 1 wins 1 point	Player 2 wins 1 point	Player 2 wins 1 point	Player 2 wins 1 point
Player 1	0	15	30	30	40	40	40	0
Player 2	0	0	0	15	15	30	40	0
Player 2 win the game

 
User Story 2 :
As a tennis referee
I want to manage the specific of the rule DEUCE at the end of a Game
In order to display the current Game score of each player
 
 Rules details:
·         If the 2 players reach the score 40, the DEUCE rule is activated
·         If the score is DEUCE , the player who  win the point take the ADVANTAGE
·         If the player who has the ADVANTAGE win the  point, he win the game
·         If the player who has the ADVANTAGE looses the point, the score is DEUCE
 
Example:
GAME SCORE
Start the game	Player 1 wins 1 point	Player 1 wins 1 point	Player 2 wins 1 point	Player 1 wins 1 point	Player 2 wins 1 point	Player 2 wins 1 point	Player 2 wins 1 point	Player 1 wins 1 point	Player 1 wins 1 point	Player 1 wins 1 point
Player 1	0	15	30	30	40	40	40	40	DEUCE	ADV	0
Player 2	0	0	0	15	15	30	40	ADV	DEUCE	40	0
Player 1 win the game

environnement pre-requis:

JDK 1.9+
MAVEN 3.6.2+
écriture des tests unitaires pour tester chaque cas des US.