import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameTest {

    private Player player1;
    private Player player2;
    private Game game;

    @Before
    public void setUp() throws Exception {
        player1 = new Player("Player1");
        player2 = new Player("Player2");
        game = new Game(player1, player2);
    }

    @Test
    public void the_game_should_start_with_a_score_of_0_point_for_each_player(){
        assertEquals(game.getPlayer1CurrentScore(), 0);
        assertEquals(game.getPlayer2CurrentScore(), 0);
    }

    @Test
    public void the_score_of_game_should_be_with_a_score_of_15_vs_0_when_player1_wins_1_point(){
        game.player1Score();
        assertEquals(game.getPlayer1CurrentScore(), 1);
        assertEquals(game.getPlayer2CurrentScore(), 0);
        assertEquals(game.getGameScore(),"15-0");
    }

    @Test
    public void the_score_of_game_should_be_with_score_of_30_vs_0_when_player1_wins_2_point(){
        game.player1Score();
        game.player1Score();
        assertEquals(game.getPlayer1CurrentScore(), 2);
        assertEquals(game.getPlayer2CurrentScore(), 0);
        assertEquals(game.getGameScore(),"30-0");
    }

    @Test
    public void the_score_of_game_should_be_with_score_of_30_vs_15_when_player2_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player2Score();
        assertEquals(game.getPlayer1CurrentScore(), 2);
        assertEquals(game.getPlayer2CurrentScore(), 1);
        assertEquals(game.getGameScore(),"30-15");
    }

    @Test
    public void the_score_of_game_should_be_with_score_of_40_vs_15_when_player1_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player1Score();
        assertEquals(game.getPlayer1CurrentScore(), 3);
        assertEquals(game.getPlayer2CurrentScore(), 1);
        assertEquals(game.getGameScore(),"40-15");
    }

    @Test
    public void the_score_of_game_should_be_with_score_of_40_vs_30_when_player2_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player1Score();
        game.player2Score();
        assertEquals(game.getPlayer1CurrentScore(), 3);
        assertEquals(game.getPlayer2CurrentScore(), 2);
        assertEquals(game.getGameScore(),"40-30");
    }

    @Test
    public void the_score_of_game_should_be_with_score_of_40_vs_40_when_player2_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player1Score();
        game.player2Score();
        game.player2Score();
        assertEquals(game.getPlayer1CurrentScore(), 3);
        assertEquals(game.getPlayer2CurrentScore(), 3);
        assertEquals(game.getGameScore(),"40-40");
    }

    @Test
    public void the_score_of_game_should_be_return_Player2_WIN_when_player2_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player1Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        assertEquals(game.getPlayer1CurrentScore(), 3);
        assertEquals(game.getPlayer2CurrentScore(), 4);
        assertEquals(game.getGameScore(),"Player2 Win the game");
    }

    @Test
    public void the_score_of_game_should_be_return_Player1_WIN_when_player1_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player1Score();
        game.player2Score();
        game.player2Score();
        game.player1Score();
        assertEquals(game.getPlayer1CurrentScore(), 4);
        assertEquals(game.getPlayer2CurrentScore(), 3);
        assertEquals(game.getGameScore(),"Player1 Win the game");
    }

    @Test
    public void should_return_DEUCE_when_the_two_players_reach_the_score_40(){
        game.player1Score();
        game.player1Score();
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        assertEquals(game.getGameScore(), "DEUCE");
    }

    @Test
    public void should_return_advantage_Player1_when_the_two_players_reach_the_score_40_and_the_score_is_DEUCE_and_player1_wins_1_point(){
        game.player1Score();
        game.player1Score();
        game.player1Score();
        game.player1Score();
        game.player1Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        assertEquals(game.getGameScore(), "ADVANTAGE Player1");
    }

    @Test
    public void should_return_advantage_Player2_when_the_two_players_reach_the_score_40_and_the_score_is_DEUCE_and_player2_wins_1_point(){
        game.player2Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        game.player2Score();
        game.player1Score();
        game.player1Score();
        game.player1Score();
        game.player1Score();
        assertEquals(game.getGameScore(), "ADVANTAGE Player2");
    }
}