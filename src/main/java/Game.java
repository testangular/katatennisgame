public class Game {
    private Player player1;
    private Player player2;

    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public int getPlayer1CurrentScore(){
        return player1.getCurrentScore();
    }

    public int getPlayer2CurrentScore(){
        return player2.getCurrentScore();
    }

    public String getGameScore(){
        if(getPlayer1CurrentScore()>3 && getPlayer2CurrentScore()>3){
            if(getPlayer1CurrentScore() == getPlayer2CurrentScore()){
                return "DEUCE";
            }
            if(getPlayer1CurrentScore() - getPlayer2CurrentScore() == 1){
                return "ADVANTAGE Player1";
            }
            if(getPlayer2CurrentScore() - getPlayer1CurrentScore() == 1){
                return "ADVANTAGE Player2";
            }
        }
        if(getPlayer1CurrentScore()>3){
            return player1.getName()+" Win the game";
        }
        if(getPlayer2CurrentScore()>3){
            return player2.getName()+" Win the game";
        }
        return player1.tennisScore()+"-"+ player2.tennisScore();
    }


    public void player1Score(){
        this.player1.score();
    }

    public void player2Score(){
        this.player2.score();
    }
}
